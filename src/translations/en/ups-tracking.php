<?php
/**
 * UpsTracking plugin for Craft CMS 3.x
 *
 * UPS tracking for world of textiles
 *
 * @link      http://mimamuh.com/
 * @copyright Copyright (c) 2018 Michael Neumair
 */

/**
 * UpsTracking en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('ups-tracking', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Michael Neumair
 * @package   UpsTracking
 * @since     0.1.0
 */
return [
    'UpsTracking plugin loaded' => 'UpsTracking plugin loaded',
];
