<?php
/**
 * UpsTracking plugin for Craft CMS 3.x
 *
 * UPS tracking for world of textiles
 *
 * @link      http://mimamuh.com/
 * @copyright Copyright (c) 2018 Michael Neumair
 */

/**
 * UpsTracking config.php
 *
 * This file exists only as a template for the UpsTracking settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'ups-tracking.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // The UPS Access Key associated with your UPS account.
    "accessKey" => "REPLACE_ME",

    // The username associated with your UPS account.
    "userId" => "REPLACE_ME",

    // The password associated with your UPS account.
    "password" => "REPLACE_ME",

    // The shipper number (account number) associated with your 
    // UPS account. Only needed when you track by reference number
    // instead of tracking number
    "shipperNumber" => "REPLACE_ME",

];
