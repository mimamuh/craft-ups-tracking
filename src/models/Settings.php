<?php
/**
 * UpsTracking plugin for Craft CMS 3.x
 *
 * UPS tracking for world of textiles
 *
 * @link      http://mimamuh.com/
 * @copyright Copyright (c) 2018 Michael Neumair
 */

namespace mimamuh\upstracking\models;

use mimamuh\upstracking\UpsTracking;

use Craft;
use craft\base\Model;

/**
 * UpsTracking Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Michael Neumair
 * @package   UpsTracking
 * @since     0.1.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * The UPS Access Key associated with your UPS account.
     *
     * @var string
     */
    public $accessKey = '';

    /**
     * The username associated with your UPS account.
     *
     * @var string
     */
    public $userId = '';

    /**
     * The password associated with your UPS account.
     *
     * @var string
     */
    public $password = '';

    /**
     * The shipper number associated with your UPS account.
     * Only needed when you track by reference number instead 
     * of tracking number
     *
     * @var string
     */
    public $shipperNumber = '';

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['accessKey', 'string'],
            ['accessKey', 'required'],
            ['accessKey', 'default', 'value' => ''],

            ['userId', 'string'],
            ['userId', 'required'],
            ['userId', 'default', 'value' => ''],

            ['password', 'string'],
            ['password', 'required'],
            ['password', 'default', 'value' => ''],

            ['shipperNumber', 'string'],
            ['shipperNumber', 'default', 'value' => ''],
        ];
    }
}
