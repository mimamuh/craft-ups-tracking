<?php
/**
 * UpsTracking plugin for Craft CMS 3.x
 *
 * UPS tracking for world of textiles
 *
 * @link      http://mimamuh.com/
 * @copyright Copyright (c) 2018 Michael Neumair
 */

namespace mimamuh\upstracking\services;

use Craft;
use Exception;
use craft\base\Component;
use mimamuh\upstracking\UpsTracking;

/**
 * UpsTracking Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Michael Neumair
 * @package   UpsTracking
 * @since     0.1.0
 */
class UpsTrackingService extends Component
{
    const BY_TRACKING_NUMBER = 'tracking-number';
    const BY_REFERENCE_NUMBER = 'reference-number';

    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     UpsTracking::$plugin->upsTracking->getShipmentByTrackingNumber()
     *
     * @return mixed
     */
    public function getShipmentByTrackingNumber($trackingNumber = '')
    {
        return self::getShipment($trackingNumber, self::BY_TRACKING_NUMBER);
    }

    /**
     * Returns the first shipment (one) by a reference number
     *
     * From any other plugin file, call it like this:
     *
     *     UpsTracking::$plugin->upsTracking->getShipmentByReferenceNumber()
     *
     * @return mixed
     */
    public function getShipmentByReferenceNumber($referenceNumber = '')
    {
        return self::getShipment($referenceNumber, self::BY_REFERENCE_NUMBER);
    }


    /**
     * Returns the shipments (multiple) by a reference number
     *
     * From any other plugin file, call it like this:
     *
     *     UpsTracking::$plugin->upsTracking->getShipmentsByReferenceNumber()
     *
     * @return mixed
     */
    public function getShipmentsByReferenceNumber($referenceNumber = '')
    {
        return self::getShipment($referenceNumber, self::BY_REFERENCE_NUMBER, true);
    }


    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     UpsTracking::$plugin->upsTracking->getShipment()
     *
     * @return mixed
     */
    public function getShipment($referenceNumber = '', $numberType = self::BY_TRACKING_NUMBER, $allowMultipleShipments = false)
    {
        $shipment = null;
        $settings = UpsTracking::$plugin->getSettings();

        if (empty($referenceNumber)) {
            Craft::info(Craft::t('ups-tracking', 'No $referenceNumber parameter passed.'), __METHOD__);
            return null;
        }

        if (empty($settings['accessKey'])) {
            Craft::error(Craft::t('ups-tracking', "Access API Key missing"), __METHOD__);
            return null;
        }

        if (empty($settings['userId'])) {
            Craft::error(Craft::t('ups-tracking', "UserId missing"), __METHOD__);
            return null;
        }

        if (empty($settings['password'])) {
            Craft::error(Craft::t('ups-tracking', "Password missing"), __METHOD__);
            return null;
        }

        if ($numberType == self::BY_REFERENCE_NUMBER && empty($settings['shipperNumber'])) {
            Craft::error(Craft::t('ups-tracking', "Shipper number is missing"), __METHOD__);
            return null;
        }

        $tracking = new \Ups\Tracking($settings['accessKey'], $settings['userId'], $settings['password']);
        
        if ($allowMultipleShipments === true) {
            // using the workaround to query for multiple shipments, which is by the ups-api
            // the default but not of the gabrielbull/php-ups-api library. This is why they added
            // a workaround with $tracking->allowMultipleShipments() to keep backward compatibility.
            // See more here: https://github.com/gabrielbull/php-ups-api/issues/117
            $tracking->allowMultipleShipments();
        }

        try {
            if ($numberType == self::BY_REFERENCE_NUMBER) {
                $tracking->setShipperNumber($settings['shipperNumber']);
                $shipment = $tracking->trackByReference($referenceNumber, 1);
            } else {
                $shipment = $tracking->track($referenceNumber);
            }
            return $shipment;
        } catch (\Ups\Exception\InvalidResponseException $error) {
            Craft::warning(Craft::t('ups-tracking', $error->getMessage()), __METHOD__);
            return null;
        } catch (\Throwable $throwable) {
            Craft::error(Craft::t('ups-tracking', $throwable->getMessage()), __METHOD__);
            return null;
        }
    }
}


