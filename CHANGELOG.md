# UpsTracking Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).


## 0.3.0 - 2018-07-25

### Added
- Added method `getShipmentsByReferenceNumber(trackingNumber)` to get multiple shippings by one reference number.



## 0.2.0 - 2018-07-12

### Added
- Added more example fields to `/examples/frontend-example.twig` and replaced depreciated code.

### Changed
- Renamed `/src/services/DefaultService.php` to `/src/services/UpsTrackingService.php`
- `/src/service/UpsTrackingService.php` logs now wrong tracking numbers as warnings instead of errors and catches unexpected exceptions seperately.

### Removed
- Old files which are no longer needed



## 0.1.0 - 2018-07-11
### Added
- Initial release

