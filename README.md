# Ups Tracking plugin for Craft CMS 3.x

![Screenshot](logo.png)

UPS tracking for Craft CMS 3.x to query the UPS Tracking API. It wrappes the [Rracking class of gabrielbull/php-ups-api](https://github.com/gabrielbull/php-ups-api#tracking-class) package and provide the api as a craft `service`:

```twig
{# Get trackingNumber from url parameter 'trackingnum' #}
{% set trackingNumber = craft.request.getParam('trackingnum') %}

{# Passes the trackingNumber to UPS tracking api #}
{% set shipment = craft.upsTracking.getShipmentByReferenceNumber(trackingNumber) %}
```



## Requirements

This plugin requires Craft CMS 3.0.0 or later.



## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require mimamuh/craft-ups-tracking

3. In the Control Panel, go to `Settings → Plugins` and click the “Install” button for `UpsTracking`.



## Configure UpsTracking

To use this plugin, you need an `UPS access key` to use their APIs. For every request, you will have to provide the Access Key, your UPS User ID and Password. You can generate your `access key` on https://www.ups.com/upsdeveloperkit/

Next in the Craft Admin CP, go to `Settings →  Plugins → UpsTracking` and enter the api credentials of your UPS site. These are the only required settings for the UpsTracking plugin.

TODO: All settings are also configurable via the `config.php` file, which is a multi-environment friendly way to store the default settings. Don't edit this file, instead copy it to `craft/config` as `ups-tracking.php` and make your changes there. `(Not yet implemented)`

```php
return [

    // The UPS Access Key associated with your UPS account.
    "accessKey" => "REPLACE_ME",

    // The username associated with your UPS account.
    "userId" => "REPLACE_ME",

    // The password associated with your UPS account.
    "password" => "REPLACE_ME",

    // The shipper number (account number) associated with your 
    // UPS account. Only needed when you track by reference number
    // instead of tracking number
    "shipperNumber" => "REPLACE_ME",

];
```

You can learn more about the `UPS Tracking API` on their page: https://www.ups.com/upsdeveloperkit/downloadresource?loc=en_DK


## Using the UpsTracking plugin in your templates

You can get UPS tracking information by a tracking number created by UPS or by a reference number 
created by the shipper. If you wanna use a reference number, you have to set the `shipperNumber` 
in the settings or `config.php` – depending which method you are using.

To get shipment information for a `tracking number`, use:
```twig
{# Get shipment by tracking number #}
{% set shipment = craft.upsTracking.getShipmentByTrackingNumber(trackingNumber) %}
```

To get shipment information for a `reference number`, use:
```twig
{# Get shipment by reference number #}
{% set shipment = craft.upsTracking.getShipmentByReferenceNumber(referenceNumber) %}
```

In case you have multiple shipments with the same `reference number`, then use:
```twig
{# Get multiple shipments by reference number #}
{% set shipments = craft.upsTracking.getShipmentsByReferenceNumber(referenceNumber) %}
{% if shipments is defined and shipments is not null %}
	{% for shipment in shipments.Shipment %}
		...
	{% endfor %}
{% endif %}
```

The result is `null`, when no entry was found (yet) or an error occured (errors are logged).

When a entry was found, the result is a `shipment` object with all the information of the shipment. You can see an example template here in [examples/frontend-example.twig](https://gitlab.com/mimamuh/craft-ups-tracking/blob/master/examples/frontend-example.twig).

```twig
{% set trackingNumber = craft.request.getParam('trackingnum') %}
{% set shipment = craft.upsTracking.getShipmentByTrackingNumber(trackingNumber) %}
{% if shipment is defined and shipment is not null %}
	<table>
		<tr>
			<td>Shipper.ShipperNumber<td>
			<td>{{ shipment.Shipper.ShipperNumber }}<td>
		</tr>
		<tr>
			<td>Shipper.Address.AddressLine1<td>
			<td>{{ shipment.Shipper.Address.AddressLine1 }}<td>
		</tr>
		<tr>
			<td>Shipper.Address.City<td>
			<td>{{ shipment.Shipper.Address.City }}<td>
		</tr>
		<tr>
			<td>Shipper.Address.PostalCode<td>
			<td>{{ shipment.Shipper.Address.PostalCode }}<td>
		</tr>
		<tr>
			<td>Shipper.Address.CountryCode<td>
			<td>{{ shipment.Shipper.Address.CountryCode }}<td>
		</tr>
	</table>

        ...

{% else %}
	<div>No entry found or error occured.</div>
{% endif %}
```


## UpsTracking Roadmap

Some things to do, and ideas for potential features:

* Allow to set `access key` with a `config.php` file.


Brought to you by [Michael Neumair](http://mimamuh.com)
